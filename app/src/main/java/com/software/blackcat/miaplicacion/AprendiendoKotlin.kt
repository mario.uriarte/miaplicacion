package com.software.blackcat.miaplicacion

import android.app.Activity
import android.os.Bundle
import android.widget.TextView

class AprendiendoKotlin : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aprendiendo_kotlin)

        //val txt = findViewByID(R.id.mensaje) as TextView
        //txt.setText("Si funciona")

        // tipos de datos
        var edadFirulais = 19

        // variable que no se puede cambiar
        val nombreFirulais: String = "linda"

        var numeroFlotante: Float = 18.5F

        var caracter: Char = '8'

        var enteroEnmascarado = caracter.toInt()

        println(edadFirulais)
        println(nombreFirulais)
        println(numeroFlotante)
        println(caracter)
        println(enteroEnmascarado)

        var cadena: String = "MarIo\n\tUriarte"

        println(cadena[0] + " " + cadena[2])
        println(cadena)

        var arreglo: Array<Int> = arrayOf(1, 2, 3)
        var arregloString: Array<String> = arrayOf("Mazda", "toyota", "nisan")

        println(arreglo[2])
        println(arregloString[1])

        var cualquierTipoDeDato: Any = "esto es una variable?"

        println(cualquierTipoDeDato)

        // operaciones aritmeticas
        var a = 40
        var b = 8

        var suma = a / b

        println(suma)

        var cadena1 = "mario"
        var cadena2 = "uriarte"

        println("Mi nombre es $cadena1 " + cadena2)
        println(cadena2.length)
        println("El numero de letras de mi apellido es: ${cadena2.length}")

        var dinero = 10
        println("$$dinero")

        // estructuras de kotlin
        estructurasDeControl()

        // sumar
        var sumaDosNumeros = sumar(5, 7)
        println("la suma es $sumaDosNumeros")

        val txt = findViewById<TextView>(R.id.mensaje)
        txt.setText(cadena1 + " " + cadena2)
    }

    fun estructurasDeControl() {

        println(">> Estructuras de control")

        val notaAlta = 3
        var resena: String

        // when
        when (notaAlta) {
            1 -> resena = "resultado 1"
            in 2..5 -> resena = "resultado 2 , 3"
            6 -> resena = "resultado 4"
            7 -> resena = "resultado 5"
            else -> resena = "resultado else"
        }

        println(resena)

        // forin
        for (i in 1..10) {
            println(i)
        }

        println("entrando al indice")
        for ((key, valor) in (10..15).withIndex()) {
            println(valor)
        }

        println("ingresando a un array")
        var arregloInt = intArrayOf(4, 2, 2, 4, 4, 2, 3, 4, 2, 3, 5, 6, 7, 8, 98, 6, 5)

        for (i in arregloInt) {
            println(i)
        }
    }

    fun sumar(a: Int, b: Int): Int {
        return a + b
    }
}
